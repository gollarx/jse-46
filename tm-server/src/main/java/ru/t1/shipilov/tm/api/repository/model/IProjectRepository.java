package ru.t1.shipilov.tm.api.repository.model;

import ru.t1.shipilov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}

