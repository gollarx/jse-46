package ru.t1.shipilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shipilov.tm.api.endpoint.IUserEndpoint;
import ru.t1.shipilov.tm.command.AbstractCommand;
import ru.t1.shipilov.tm.exception.entity.UserNotFoundException;
import ru.t1.shipilov.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return getServiceLocator().getAuthEndpoint();
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
